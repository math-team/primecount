#!/bin/sh
# autopkgtest check
set -ue
cd $AUTOPKGTEST_TMP

status=0

echo ""

# Test 1
res=$(primecount 10^10)
echo "Count primes using default algorithm: Primes <= 10^10: $res"

if [ "$res" -ne "455052511" ]; then
	echo "Error: Prime count test: correct result is 455052511" 1>&2
	status=1
fi

# Test 2
res=$(primecount 524309 --lehmer)
echo "Count primes using Lehmer's formula: Primes <= 524309: $res"

if [ "$res" -ne "43391" ]; then
	echo "Error: Prime count test: correct result is 43391" 1>&2
	status=1
fi

# Test 3
res=$(primecount 2**31 --legendre)
echo "Count primes using Legendre's formula: Primes <= 2^31: $res"

if [ "$res" -ne "105097565" ]; then
	echo "Error: Prime count test: correct result is 105097565" 1>&2
	status=1
fi

# Test 4
res=$(primecount 2**32 --meissel)
echo "Count primes using Meissel's formula: Primes <= 2**32: $res"

if [ "$res" -ne "203280221" ]; then
	echo "Error: Prime count test: correct result is 203280221" 1>&2
	status=1
fi

# Test 5
res=$(primecount 2**32+15 --deleglise-rivat)
echo "Count primes using the Deleglise-Rivat algorithm: Primes <= 2**32+15: $res"

if [ "$res" -ne "203280222" ]; then
	echo "Error: Prime count test: correct result is 203280222" 1>&2
	status=1
fi

# Test 6
res=$(primecount 2**32-17 --primesieve)
echo "Count primes using the sieve of Eratosthenes: Primes <= 2**32-17: $res"

if [ "$res" -ne "203280220" ]; then
	echo "Error: Prime count test: correct result is 203280220" 1>&2
	status=1
fi

# Test 7
res=$(primecount 10^9 -n)
echo "Calculate the 10^9 th prime: $res"

if [ "$res" -ne "22801763489" ]; then
	echo "Error: Nth prime test: correct result is 22801763489" 1>&2
	status=1
fi

echo ""
echo "All tests passed successfully!"

exit $status
