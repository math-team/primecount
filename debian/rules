#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

## inspired from mpfr material
ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
	CFLAGS    += -O0
	CXXCFLAGS += -O0
else ifeq ($(DEB_HOST_ARCH),alpha)
	CFLAGS   += -mieee
	CXXFLAGS += -mieee
else ifeq ($(DEB_HOST_ARCH),sha4)
	CFLAGS   += -mieee
	CXXFLAGS += -mieee
else
	CFLAGS   := $(subst -O2,-O3,$(CFLAGS))
	CXXFLAGS := $(subst -O2,-O3,$(CXXFLAGS))
endif

default:
	@uscan --no-conf --dehs --report || true

%:
	dh $@ --builddirectory=_BUILD

override_dh_auto_configure:
	dh_auto_configure -- \
		-DWITH_POPCNT=OFF \
		-DBUILD_LIBPRIMESIEVE=OFF \
		-DBUILD_SHARED_LIBS=ON \
		-DBUILD_STATIC_LIBS=ON \
		-DBUILD_TESTS=ON \
		-DBUILD_MANPAGE=ON

override_dh_auto_build-indep: README.toc.md
	$(MAKE) -f debian/adhoc/Makefile doc

override_dh_auto_test-indep:
	true

override_dh_auto_install-indep:
	DESTDIR=$(CURDIR)/debian/tmp $(CMAKE) \
			-DCMAKE_INSTALL_COMPONENT=libprimecount-headers \
		-P _BUILD/cmake_install.cmake

override_dh_prep-arch: README.toc.md

override_dh_installdocs:
	dh_installdocs -pprimecount-bin    --link-doc=libprimecount7
	dh_installdocs -plibprimecount-dev --doc-main-package=libprimecount-dev-common
	dh_installdocs --remaining-packages

override_dh_compress-indep:
	dh_compress -X.pdf -X.md -Xexamples

override_dh_missing:
	dh_missing --list-missing

README.toc.md:
	sed \
			-e '/\[libprimecount/d' \
			-e '/\[primecount-backup/d' \
			-e '/\[BUILD/d' \
			-e '/\[RELEASE/d' \
		doc/README.md > README.toc.md

CMAKE ?= cmake
